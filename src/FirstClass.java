//The class to hold our main method
public class FirstClass {

    //The main method, the "entry" point for your program
    //Not ALL programs need a main method, but most programs require it
    //This is where your program starts running
    public static void main(String[] args) {

        //Print a message to the console
        System.out.println("Hello World"); //Hello World is a String

        //You can print many other things to the console, like numbers
        System.out.println(19);
        System.out.println(14 + 5);

        //You can even combine Strings and numbers
        System.out.println("The amount of people I've killed: " + 27);

        //Using the arguments provided when running the program
        if (args.length == 1){
            System.out.println("Hello, " + args[0]);
        }

    }
}
